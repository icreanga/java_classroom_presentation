package com.endava.java_classroom;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.System.*; //printstream out is static member for System class

public class TestClass1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		out.println(MAX_VALUE);
		for (String argument : args) {
			out.println(argument);
		}
		
		getProperties().list(out);
		
	}

}
